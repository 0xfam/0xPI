# 0xPI

> Built With ❤

## Introduction

The Official Repo for the 0xPI Web3 CMS and Community.
This is an Experiemental Web2/3 Website.

## Whats Inside

- Docker
- NextJS/ReactJS
- MongoDB
- Ganache-cli
- Nginx
- Rimble UX/UI

## Getting Started

First, run the development server:

```bash
docker-compose -f docker-compose.yml up
# or
docker-compose -f docker-compose.yml up --build
```

Open [http://localhost:3001](http://localhost:3001) with your browser to see the result.

## Documentation

#### Dev Tools

- [Docker](docker.com)

#### Web3

- [MetaMask](https://docs.metamask.io)
- [Web3.js](https://web3js.readthedocs.io/)
- [Infura](https://infura.io/docs)
- [Ganache-cli](https://github.com/trufflesuite/ganache-cli/blob/master/README.md)

#### Frontend

- [Next.js](https://nextjs.org)
- [React.js](https://reactjs.org)
- [Rimble](https://rimble.consensys.design/)

#### Database

- [Mongo DB](https://docs.mongodb.com/)
