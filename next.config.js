const withPWA = require('next-pwa');

const runtimeCaching = require('next-pwa/cache');
const nextConfig = {
  devIndicators: {
    autoPrerender: true,
  },
  pwa: {
    dest: 'public',
    runtimeCaching,
  },
  webpackDevMiddleware: (config) => {
    // Solve compiling problem via vagrant
    config.watchOptions = {
      poll: 1000, // Check for changes every second
      aggregateTimeout: 300, // delay before rebuilding
    };
    return config;
  },
  async rewrites() {
    return [
      {
        source: '/dashboard/:path*',
        destination: '/Dashboard/:path*',
      },
      {
        source: '/:path*/Settings',
        destination: '/:path*/settings',
      },
      {
        source: '/Live',
        destination: '/live',
      },
      {
        source: '/Live/:path*',
        destination: '/live/:path*',
      },
      {
        source: '/Minecraft',
        destination: '/minecraft',
      },
      {
        source: '/Wallet',
        destination: '/wallet',
      },
      {
        source: '/About',
        destination: '/about',
      },
    ];
  },
  env: {
    ADMIN_ID: process.env.ADMIN_ID,
    INFURA_API: process.env.INFURA_API,
    SPACES: process.env.SPACES,
    ETHERSCAN_API: process.env.ETHERSCAN_API,
  },
};

module.exports =
  process.env.NODE_ENV === 'development' ? nextConfig : withPWA(nextConfig);
