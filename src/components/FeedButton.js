export default function FeedButton(props) {
  const { feedButtonContainerStyle } = props;
  return (
    <span
      className={`feed-button border border-left-0 border-dark ${
        feedButtonContainerStyle !== undefined ? feedButtonContainerStyle : ''
      }`}>
      <Button.Base
        height={'100%'}
        color={'black'}
        onClick={() => props.onPress()}>
        Submit
      </Button.Base>
    </span>
  );
}
