export default function HomeHeader({
  homeHeaderStyle = '',
  mainTextStyle = 'display-2',
  subTextStyle = '',
  address,
  children,
}) {
  return (
    <div className={`d-flex flex-column p-5 ${homeHeaderStyle}`}>
      <style jsx>{``}</style>
      <div
        className={
          'd-flex flex-column justify-content-center align-items-center'
        }>
        <p className={mainTextStyle}>Welcome To 0xPI</p>
        {/* <p className={subTextStyle}>
          0xPI is a community focused on building decentralized applications and
          Web3 Content.
        </p>
        <p className={subTextStyle}>
          This is an Experimental website utilizing various api to bring what
          web3 has to offer you, so please use with caution.
        </p> */}
      </div>
      {children}
    </div>
  );
}
