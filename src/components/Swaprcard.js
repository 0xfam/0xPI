import { useState, useEffect } from 'react';
import { Input, Card, Field } from 'rimble-ui';

import { getQuote, submitTransaction } from '../lib';
import Select from './Select';

function SwaprCard({
  options = [],
  defaultSaleSymbol = 'ETH',
  defaultBuySymbol = 'DAI',
  error,
}) {
  const [saleSymbol, setSaleData] = useState({});
  const [buySymbol, setBuyData] = useState({});
  const [saleValue, setSaleValue] = useState(0);
  const [buyValue, setBuyValue] = useState(0);
  const [quote, setQuote] = useState(null);
  const [show, setShow] = useState(false);
  const [quoteValue, setQuoteValue] = useState(0);

  useEffect(async () => {
    if (quote !== null) setShow(true);
    setTimeout(() => {
      setShow(false);
    }, 30000);
  }, [quote]);

  useEffect(() => {
    setQuote(null);
    setShow(false);
  }, [saleValue, buyValue]);

  useEffect(() => {
    let arr = options.filter(
      (opt) => opt.label == defaultSaleSymbol || opt.label == defaultBuySymbol
    );
    if (arr.length > 0) {
      setSaleData(arr[1].value);
      setBuyData(arr[0].value);
    }
  }, [options]);

  return (
    <Card
      p={5}
      pt={3}
      mx={2}
      width={'350px'}
      height={'100%'}
      maxHeight={'375px'}>
      <style jsx>
        {`
          input.Box__StyledBox-ha1bw0-0.Input__StyledInput-vwozih-0.w-100.cQplnW.jEAKTz {
            width: 100%;
          }
        `}
      </style>
      <div className="d-flex flex-column align-items-cener">
        <Field label="SWAP ETH">
          <Input
            type="number"
            required={true}
            placeholder="0"
            value={saleValue}
            onChange={async (e) => {
              if (e.target.value >= 0 && e.target.value < 1000) {
                await setSaleValue(e.target.value);
              }
            }}
          />
        </Field>

        <div className={'d-flex flex-row h-100'}>
          <Select
            title="Coming Soon"
            disable={true}
            defaultValue={saleSymbol}
            onChange={(symbol) => setSaleData(JSON.parse(symbol))}
            options={options}
            selectStyle={'border-none'}
          />

          <p
            className={
              'text-center my-1 border border-top-0 border-bottom-0 py-2 mx-2 h-100 col'
            }>
            To
          </p>

          <Select
            defaultValue={buySymbol}
            onChange={(symbol) => setBuyData(JSON.parse(symbol))}
            options={options}
            selectStyle={'border-none'}
          />
        </div>
      </div>

      <button
        className="w-100 btn m-0 py-2 mt-2 border border-top"
        onClick={async () => {
          if (parseInt(saleValue) > 0) {
            let _quote = await getQuote(
              saleSymbol.symbol,
              buySymbol.symbol,
              parseInt(saleValue),
              saleSymbol.decimals
            );
            await setQuote(_quote);
            if (_quote !== undefined) await setQuoteValue(_quote.price);
          }
        }}>
        Get Quote
      </button>

      <p className="ml-2 mt-2 pt-2 text-uppercase">
        {quoteValue * saleValue || 0} {saleSymbol.symbol}
        <br />
        To ~ {saleValue || 0} {buySymbol.symbol} <br /> + (0.075% Transfer fee)
        <br />~ 3.77% Slippage
      </p>
      <p className="small my-3 text-capitalize">
        {' '}
        <a
          className="fnt-color-black"
          href="https://0x.org/docs/api"
          target="_blank">
          powered by 0x API
        </a>
      </p>
      {show ? (
        <button
          className="w-100 btn m-0 py-2 border border-dark border-top"
          onClick={async () => {
            submitTransaction(quote);
          }}>
          Accept Quote (30 SEC)
        </button>
      ) : null}
      <span>{error}</span>
    </Card>
  );
}
export default SwaprCard;
