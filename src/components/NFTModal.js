export default function NFTModal({
  id = '',
  name = '',
  permalink = '',
  animation_url = '',
  external_link = '',
  image_preview_url = '',
  description = '',
  collection = {},
  sell_orders,
  containerStyle = '',
  onClose = () => {
    return;
  },
}) {
  return (
    <div className={`${containerStyle} mt-5`}>
      <div
        className={`nft-modal h-100 w-100  position-absolute `}
        onClick={() => {
          onClose();
        }}>
        <style jsx global>
          {`
            .overlay {
              position: fixed;

              top: 0;
              left: 0;
              width: 100%;
              height: 100%;
              background: #0000003a;
              transition: opacity 0.2s ease;
              z-index: 99;
            }
            .nft-modal {
              background: #fff;
              max-width: 800px;
              margin-left: auto;
              margin-right: auto;
              left: 0;
              right: 0;
            }
            .nft-modal-img {
              max-height: 400px;
              max-width: 400px;
            }
          `}
        </style>

        <div
          className={` d-flex flex-column justify-content-center align-items-center h-100 w-100 p-5 border border-dark`}
          onClick={(e) => e.stopPropagation()}>
          <div
            className={`btn ml-auto mt-3 py-3 px-5 position-absolute end-0 top-0`}
            onClick={() => onClose()}>
            x
          </div>
          <div className={'nft-modal-img h-100 w-100 border border-dark'}>
            {animation_url !== null &&
            animation_url.length !== 0 &&
            !animation_url.includes('.mp3') ? (
              <video
                autoplay
                controls
                className={'h-100 w-100'}
                src={animation_url}
              />
            ) : (
              <img
                className={'h-100 w-100'}
                src={image_preview_url}
                alt={name}
              />
            )}
          </div>
          <hr />
          <div className={`text-left mx-auto w-100`}>
            <p>Title: {name}</p>
            <p>Collection: {collection.name}</p>
            <p>{description}</p>
            {collection.external_url && (
              <a href={collection.external_url} target={`_new`}>
                External Link
              </a>
            )}
          </div>
          <hr />
          <div className={`text-left mx-auto w-100 my-2 border p-2`}>
            <p>Orders:</p>
            {sell_orders && (
              <ol>
                {sell_orders.map(({ current_price, quantity }, key) => {
                  return (
                    <li
                      key={key}
                      className={`border-bottom border-dark d-flex flex-row justify-content-between`}>
                      <div className={`d-flex flex-column `}>
                        <span>
                          Price: {parseInt(current_price) / Math.pow(10, 18)}
                        </span>
                        <span>Quantity:{quantity}</span>
                      </div>
                      <div>
                        <span
                          onClick={() => alert('Coming Soon')}
                          className={`btn border border-dark`}>
                          Buy
                        </span>
                      </div>
                    </li>
                  );
                })}
              </ol>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
