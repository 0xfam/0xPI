export default function Select({
  title = '',
  disable = false,
  dropDownStyle = '',
  options = [],
  ariaLabel = '',
  onChange = () => {
    console.log('updating Value');
  },
  defaultValue = '',
}) {
  return (
    <select
      title={title}
      disabled={disable}
      value={defaultValue}
      className={`col ${dropDownStyle}`}
      aria-label={ariaLabel}
      onChange={(e) => {
        e.preventDefault();
        onChange(e.target.value);
      }}>
      <style jsx>
        {`
          select {
            border: none;
          }
          .dropdown {
            overflow: hidden;
            height: 25px;
          }
        `}
      </style>
      {options.map(({ label = '', value = '' }, key) => {
        value = JSON.stringify(value);
        return (
          <option key={key} className={'dropdown-item'} value={value}>
            {label}
          </option>
        );
      })}
    </select>
  );
}
