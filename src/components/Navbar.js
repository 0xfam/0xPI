import NavItem from './common/NavItem';
import { useRouter } from 'next/router';
import LoginButton from '@/components/LoginButton';
import { connect } from 'react-redux';
import { toggleSidebar } from '../actions';
function SvGButton(props) {
  const {
    onPress = () => {
      return;
    },
  } = props;
  return (
    <svg
      onClick={() => onPress()}
      width="1em"
      height="1em"
      viewBox="0 0 16 16"
      className="svg-button bi bi-octagon ml-1 mr-5 fnt-24 rotate fnt-color-000"
      fill="currentColor"
      xmlns="http://www.w3.org/2000/svg">
      <style jsx>
        {`
          svg,
          .svg-button {
            cursor: pointer;
            width: 1.5rem;
          }
          .rotate {
            animation: rotation 8s infinite linear;
          }

          @keyframes rotation {
            from {
              transform: rotate(0deg);
            }
            to {
              transform: rotate(359deg);
            }
          }
        `}
      </style>
      <path
        fillRule="evenodd"
        d="M4.54.146A.5.5 0 0 1 4.893 0h6.214a.5.5 0 0 1 .353.146l4.394 4.394a.5.5 0 0 1 .146.353v6.214a.5.5 0 0 1-.146.353l-4.394 4.394a.5.5 0 0 1-.353.146H4.893a.5.5 0 0 1-.353-.146L.146 11.46A.5.5 0 0 1 0 11.107V4.893a.5.5 0 0 1 .146-.353L4.54.146zM5.1 1L1 5.1v5.8L5.1 15h5.8l4.1-4.1V5.1L10.9 1H5.1z"
      />
    </svg>
  );
}
function Navbar(props) {
  const router = useRouter();
  const { toggleSidebar, session } = props;
  function handleSidebar() {
    toggleSidebar(!session.showSidebar);
  }
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light sticky-top border-top border-bottom border-dark px-4">
      <style jsx>
        {`
          .navbar {
            z-index: 300;
          }
          .navbrand-container {
            width: 100px;
          }
          .navbar-brand {
            cursor: pointer;
          }

          .rotate {
            animation: rotation 8s infinite linear;
          }

          @keyframes rotation {
            from {
              transform: rotate(0deg);
            }
            to {
              transform: rotate(359deg);
            }
          }
        `}
      </style>
      <div className={'d-flex flex-row w-100'}>
        <div className={`d-flex flex-row align-items-center`}>
          <SvGButton onPress={() => handleSidebar()} />
          <a
            className="navbar-brand ps-2 pe-2"
            onClick={(e) => {
              e.preventDefault();
              router.push('/');
            }}>
            0xPI
          </a>
          {/* 
          <div
            id="navbarSupportedContent"
            className="collapse navbar-collapse navbar-nav">
            <div className={`d-flex flex-row flex-wrap`}>
              <NavItem Route="/hub">Hub</NavItem>
              <NavItem Route="/collection">Collection</NavItem>
              <NavItem Route="/gallery">Gallery</NavItem>
              <NavItem Route="/about">About</NavItem>
              <NavItem Route="/live">Live</NavItem>
              <NavItem externalLink={'https://gitlab.com/0xfam/0xPI'}>
                Our Code
              </NavItem>
            </div> */}
          {/* </div> */}
        </div>
        <div className="ms-auto d-flex flex-row justify-content-end align-items-center">
          {/* <button
            className="navbar-toggler mx-2"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button> */}
          <LoginButton />
        </div>
      </div>
    </nav>
  );
}
const mapStateToProps = (state) => ({
  session: state.session,
});
export default connect(mapStateToProps, { toggleSidebar })(Navbar);
