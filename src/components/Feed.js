function Feed(props) {
  return (
    <div className={`feed `}>
      <hr />
      {Array.isArray(props.data) && props.data.length >= 1 ? (
        <div className={`h-100 feed-items`}>
          {props.data
            .sort((a, b) => b.nonce - a.nonce)
            .map(({ address, date, text }, i) => {
              return (
                <FeedItem
                  key={i}
                  address={address}
                  date={date}
                  content={text}
                />
              );
            })}
        </div>
      ) : (
        <div />
      )}
      <hr />
    </div>
  );
}
export default Feed