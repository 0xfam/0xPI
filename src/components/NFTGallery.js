import NFTCard from '@/components/NftCard';
export default function NFTGallery({
  address = '',
  nftArray = [],
  onClick = () => {
    return;
  },
}) {
  return (
    <div
      className={
        'nft-gallery container d-flex flex-row flex-wrap justify-content-lg-between justify-content-center w-100 px-5 mx-auto mb-5'
      }>
      {nftArray.length > 0
        ? nftArray.map(
            (
              {
                id,
                permalink,
                external_link,
                name,
                description,
                image_preview_url,
                animation_url,
                creator,
                sell_orders,
                collection,
                //: {
                // address,
                // profile_img_url,
                // user: { username },
                //},
                owner,
                // {
                // address,
                // profile_img_url,
                // user: { username },
                //},
              },
              key
            ) => {
              return (
                <NFTCard
                  key={key}
                  id={id}
                  name={name}
                  // permalink={permalink}
                  animation_url={animation_url}
                  // external_link={external_link}
                  image_preview_url={image_preview_url}
                  description={description}
                  onClick={() =>
                    onClick({
                      name,
                      description,
                      animation_url,
                      image_preview_url,
                      collection,
                      sell_orders,
                    })
                  }
                />
              );
            }
          )
        : 'Loading Your Collectables...'}
    </div>
  );
}
