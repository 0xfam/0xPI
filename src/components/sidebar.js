import { useState } from 'react';
import { connect } from 'react-redux';
import { NavItem } from './common/';
function SideBar({ session: { address, showSidebar } }) {
  const [showMenuState, setShowMenuState] = useState(true);

  return (
    <div
      className={` ${
        !showSidebar ? 'hide' : 'sidebar'
      } d-flex flex-column border-end border-dark h-100 position-absolute mx-3`}>
      <style global jsx>
        {`
          .hide {
            width: 10px;
          }
          .sidebar {
            z-index: 3;
            background-color: #fff;
            width: 10rem;
          }
          .sidebar-nav a {
            color: #000;
          }
          .sidebar-menu {
          }
          .sidebar-menu-text {
            cursor: pointer;
          }

          @media screen and (max-width: 576px) {
            .hide {
              width: 0rem;
            }
            .sidebar {
              width: 100%;
            }
            .border-right {
              border-right: none !important;
            }
          }
        `}
      </style>
      {showSidebar ? (
        <div className={`sidebar-menu d-flex flex-column `}>
          <span>Click Hex To Close</span>
          <p
            title={`Click Me ❤`}
            className={`sidebar-menu-text ml-4 mt-5 border-bottom border-dark`}
            onClick={() => {
              setShowMenuState(!showMenuState);
            }}>
            Menu:
          </p>

          <ul
            className={`${
              !showMenuState ? 'd-none' : ''
            } navbar-nav sidebar-nav me-auto flex-column`}>
            <NavItem Route="/hub">Hub</NavItem>
            <NavItem Route="/collection">Collection</NavItem>
            <NavItem Route="/gallery">Gallery</NavItem>
            <NavItem Route="/about">About</NavItem>
            <NavItem Route="/live">Live</NavItem>
            <NavItem externalLink={'https://gitlab.com/0xfam/0xPI'}>
              Our Code
            </NavItem>
          </ul>
        </div>
      ) : (
        <div />
      )}
    </div>
  );
}
const mapStateToProps = (state) => ({
  session: state.session,
});
export default connect(mapStateToProps)(SideBar);
