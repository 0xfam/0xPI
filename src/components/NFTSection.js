import { useEffect, useState, useRef } from 'react';

import NFTGallery from '@/components/NFTGallery';
import NFTModal from '@/components/NFTModal';
import { connect } from 'react-redux';
// import _0xDao from '../dao';
// const { getNFTSByAddress } = _0xDao;

function NFTSection({ address,nft_array=[] }) {
  const [showModal, setShowModal] = useState(false);
  // const [assetState, setAssetState] = useState([]);
  const [nftImgUrl, setNftImgUrl] = useState('');
  const [nftAnimationUrl, setNftAnimationUrl] = useState('');
  const [nftName, setNftName] = useState('');
  const [nftDescription, setNftDescription] = useState('');
  const [nftCollection, setNftCollection] = useState({});
  const [nftSaleOrder, setNftSaleOrder] = useState({});
  async function handleModal({
    name,
    description,
    animation_url,
    image_preview_url,
    collection,
    sell_orders,
  }) {
    try {
      await setNftName(name);
      await setNftDescription(description);
      await setNftAnimationUrl(animation_url);
      await setNftImgUrl(image_preview_url);
      await setNftCollection(collection);
      await setNftSaleOrder(sell_orders);
      await setShowModal(true);
    } catch (error) {
      console.log(`error handleModal()=>: ${error}`);
    }
  }
  return (
    <div>
      {nft_array.length > 0 && (
        <NFTGallery
          address={address}
          nftArray={nft_array}
          onClick={({
            name,
            description,
            animation_url,
            image_preview_url,
            collection,
            sell_orders,
          }) =>
            handleModal({
              name,
              description,
              animation_url,
              image_preview_url,
              collection,
              sell_orders,
            })
          }
        />
      )}
      {showModal && (
        <NFTModal
          containerStyle={showModal && 'overlay'}
          onClose={() => setShowModal(false)}
          image_preview_url={nftImgUrl}
          animation_url={nftAnimationUrl}
          description={nftDescription}
          name={nftName}
          sell_orders={nftSaleOrder}
          collection={nftCollection}
        />
      )}
    </div>
  );
}
const mapStateToProps = (state) => ({
  address: state.session.address,
});
export default connect(mapStateToProps)(NFTSection);
