export default function FeedInput(props) {
  const { feedInputStyle, address } = props;
  return (
    <div
      className={`feed-input d-flex flex-column m-0 p-3 border border-dark position-relative ${feedInputStyle}`}>
      <p>
        <strong>
          <u>
            {truncateAddress(Array.isArray(address) ? address[0] : address)}
          </u>
        </strong>
      </p>
      <FeedTextArea
        textAreaStyle={`position-relative`}
        value={props.value}
        onChange={(e) => props.onChange(e)}>
        <span
          className={`feed-text-length px-3 position-absolute bottom-0 end-0`}>
          {props.value.length}/280
        </span>
      </FeedTextArea>
      <FeedButton
        feedButtonContainerStyle={`mt-3 ms-auto`}
        onPress={() => props.onPress()}
      />
    </div>
  );
}
