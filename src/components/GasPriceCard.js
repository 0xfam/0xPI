import { useState, useEffect } from 'react';
import { getGasPrices } from 'lib';
export default function GasPriceCard(props) {
  const [gasPrice, setGasPriceState] = useState({});
  useEffect(async () => {
    setGasPriceState(await getGasPrices());
  }, []);

  return (
    <div
      className={`gas-price-card m-2 p-5 d-flex flex-column text-lefft justify-content-center`}>
      <style jsx>
        {`
          .gas-price-card {
            width: 250px;
            height: 200px;
            box-sizing: border-box;
            background-color: rgb(255, 255, 255);
            border: 1px solid rgb(238, 238, 238);
            box-shadow: rgb(0 0 0 / 10%) 0px 8px 16px;
          }
        `}
      </style>
      <div className={'mx-auto'}>
        <p className={'h5'}> Ethereum Gas Prices</p>
        <p className={'text-uppercase text-success'}>
          Safe Gas Price: {gasPrice.SafeGasPrice}
        </p>
        <p className={'text-uppercase text-primary'}>
          Propose Gas Price: {gasPrice.ProposeGasPrice}
        </p>
        <p className={'text-uppercase text-danger'}>
          Fast Gas Price: {gasPrice.FastGasPrice}
        </p>
      </div>
    </div>
  );
}
