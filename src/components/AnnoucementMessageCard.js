import { truncateAddress } from 'lib';

export default function AnnoucementMessageCard(props) {
  const { address, onClose, show = true, containerStyle = '' } = props;

  return show ? (
    <div
      className={`annoucement-message p-5 pt-4 border border-dark position-relative ${containerStyle}`}>
      <style global jsx>
        {`
          .annoucement-message {
            width: 100%;
            max-height: 200px;
            box-sizing: border-box;
            padding: 32px 64px 64px;
            margin-left: 8px;
            margin-right: 8px;

            background-color: rgb(255, 255, 255);
            border: 1px solid rgb(238, 238, 238);
            box-shadow: rgb(0 0 0 / 10%) 0px 8px 16px;
          }
          .address {
            width: 100px;
          }
        `}
      </style>
      <button onClick={onClose} className={`btn position-absolute top-0 end-0`}>
        X
      </button>
      <div className={'text-center'}>

      <p className={`h1 text-capitalize`}>
        Welcome{' '}
        <span title={`${address}`} className={`address text-truncate`}>
          {address !== null &&
          address !== undefined &&
          address.length !== 0 &&
          address[0] !== undefined ? (
            <strong>
              <u>
                {truncateAddress(Array.isArray(address) ? address[0] : address)}
              </u>
            </strong>
          ) : (
            'To 0xPI'
          )}
        </span>
        ,
      </p>
      <p className={`text-capitalize fnt-size-16`}>
        This is a Portal into the world of Web3.
      </p>
      <p className={`text-capitalize fnt-size-16`}>
        This is an Experimental website utilizing various api to bring what web3
        has to offer you, so please use with caution.
      </p>
      {!(
        address !== null &&
        address !== undefined &&
        address.length !== 0 &&
        address[0] !== undefined
      ) ? (
        <div className="w-100">
          <p className={`text-capitalize fnt-size-16`}>
            To have a full experience,{' '}
            <strong>
              <u>Please connect your Ethereum wallet</u>
            </strong>{' '}
            <svg
              width="1em"
              height="1em"
              viewBox="0 0 16 16"
              className="bi bi-arrow-up-right-circle-fill"
              fill="currentColor"
              xmlns="http://www.w3.org/2000/svg">
              <path
                fillRule="evenodd"
                d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.879 10.828a.5.5 0 1 1-.707-.707l4.096-4.096H6.5a.5.5 0 0 1 0-1h3.975a.5.5 0 0 1 .5.5V9.5a.5.5 0 0 1-1 0V6.732l-4.096 4.096z"
              />
            </svg>
          </p>
          <p className={`text-capitalize fnt-size-16`}>
            <i>
              or follow the on-boarding process using the instructions above.
            </i>
          </p>
          <p className={`text-capitalize fnt-size-16`}></p>
        </div>
      ) : (
        <div />
      )}
      </div>
    </div>
  ) : (
    <div
      className={`annoucement-message-collapse border border-dark position-relative ${containerStyle}`}>
      <style jsx>
        {`
          .annoucement-message-collapse {
            width: 100%;
            height: 25px;
            padding: 1.35rem;
          }
          .address {
            width: 100px;
          }
        `}
      </style>
      <button onClick={onClose} className={`btn position-absolute top-0 end-0`}>
        V
      </button>
    </div>
  );
}
