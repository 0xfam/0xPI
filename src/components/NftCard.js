function NFTCard({
  id = '',
  name = '',
  permalink = '',
  animation_url = '',
  external_link = '',
  image_preview_url = '',
  description = '',
  onClick = () => {
    return;
  },
}) {
  return (
    <div
      id={id}
      className={'nft-card h-100 m-3 border border-dark'}
      onClick={() => onClick()}>
      <style jsx>
        {`
          .nft-card {
            width: 20rem !important;
            // max-height: 20rem;
            box-shadow: 10px 5px #000;
          }
          .nft-card-img {
            min-height: 6.25rem;
            width: 14.0625rem;
            height: 15.625rem;
          }
          .nft-card-text {
            width: 20rem !important;
            height: 75px;
          }

          img {
            object-fit: contain;
          }
        `}
      </style>
      <span
        className={
          'nft-card-title d-block p-1 text-break border-bottom border-dark'
        }>
        {name}
      </span>
      {/* <a href={permalink} target={'_blank'}> */}
      <div className={'nft-card-img pt-3 pb-2 mx-auto'}>
        {(animation_url !== null && !animation_url.includes('.mp3')) ||
        image_preview_url.includes('.mp4') ? (
          <video
            autoplay
            controls
            className={'h-100 w-100'}
            src={animation_url}
          />
        ) : (
          <img className={'h-100 w-100'} src={image_preview_url} alt={name} />
        )}
      </div>
      {/* </a> */}
      <div
        className={
          'nft-card-text d-flex flex-row align-items-center border-top border-dark mt-1 p-2 h-100 w-100'
        }>
        <p className={'text-break text-truncate col m-0'}>{description}</p>
        {external_link !== null ? (
          <a
            target={'_blank'}
            href={external_link}
            className={'btn btn-outline-dark col-2'}>
            {' '}
            |Ξ
          </a>
        ) : (
          ''
        )}
      </div>
    </div>
  );
}
export default NFTCard;
