import { useRef, useEffect } from 'react';
import { useRouter } from 'next/router';

function NavItem(props) {
  let {
    navItemStyle = '',
    navLinkStyle = '',
    Route = '',
    externalLink = '',
  } = props;
  const router = useRouter();
  const link = useRef(null);
  const handleClick = (e) => {
    // e.preventDefault();
    router.push(Route);
  };
  useEffect(() => {
    if (externalLink.length > 0) {
      link.current.href = 'https://gitlab.com/0xfam/0xPI';
      link.current.target = '_new';
    }
  }, []);

  return (
    <div className={`${navItemStyle} nav-item mx-3`}>
      <style jsx>
        {`
          .nav-item {
            cursor: pointer;
          }
        `}
      </style>
      <a
        ref={link}
        className={`nav-link fnt-color-black ${navLinkStyle} `}
        onClick={() => handleClick()}>
        {props.children}
      </a>
    </div>
  );
}
export default NavItem;
