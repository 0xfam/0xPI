import { truncateAddress } from 'lib';
export default function FeedItem(props) {
  const { address, date, content } = props;
  return (
    <div
      className={`feed-item rounded mx-auto d-flex flex-column border border-dark p-3 my-3`}>
      <style jsx>
        {`
          .feed-item {
            max-width: 650px;
          }
        `}
      </style>
      <div className={`d-flex flex-column`}>
        <p>{truncateAddress(address)}</p>
        <p>Date: {date}</p>
      </div>
      <div>{content}</div>
      <hr />
      <div className={`d-flex flex-row`}>
        <button
          onClick={() => {
            console.log(`Liked Post`);
          }}
          className={`mr-4`}>
          {' '}
          Like
        </button>{' '}
        {` `}
        <button
          onClick={() => {
            console.log(`Disliked Post`);
          }}
          className={`mr-4`}>
          Dislike
        </button>
      </div>
    </div>
  );
}
