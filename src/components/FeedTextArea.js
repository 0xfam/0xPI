export default function FeedTextArea(props) {
  return (
    <div className={`text-area ${props.textAreaStyle}`}>
      <textarea
        className={`text-area w-100 p-3 `}
        placeholder={`What's The Move?`}
        rows={4}
        onChange={(e) => {
          props.onChange(e.target.value);
        }}
        value={props.value}
      />
      {props.children}
    </div>
  );
}
