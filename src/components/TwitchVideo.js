const { useEffect, useState } = require('react');
import dynamic from 'next/dynamic';
import moifetch from 'moifetch';
import { Button, Input } from 'rimble-ui';
function _TwitchVideo({
  twitchVideoStyle = '',
  user = 'moikapy',
  chat = true,
  Size = 'S',
  onPress = () => {
    console.log('please add function');
  },
}) {
  const [isLoading, setIsLoading] = useState(true);
  const [newUser, setNewUser] = useState('');
  const [updateStream, setUpdateStream] = useState('');
  async function getTwitchEmbed() {
    try {
      let livePage = await document.body;
      let script = await document.createElement('script');
      script.src = 'https://embed.twitch.tv/embed/v1.js';
      await livePage.appendChild(script);
    } catch (error) {}
  }
  useEffect(() => {
    getTwitchEmbed();
    setIsLoading(false);
    setNewUser(user);
  }, []);
  useEffect(async () => {
    // if (isDev) {
    const myNode = document.getElementById('twitch-embed');
    myNode.innerHTML = '';
    setTimeout(() => {
      if (typeof Twitch !== 'undefined' && isLoading === false) {
        new Twitch.Embed('twitch-embed', {
          width: '100%',
          height: '100%',
          channel: newUser,
          parent: ['www.0xpi.xyz', '0xpi.xyz'],
          layout: chat ? 'video-with-chat' : 'video',
        });
      }
    }, 500);
    // }
  }, [isLoading, updateStream]);
  return (
    <div className={`d-flex flex-column p-2`}>
      <style jsx>
        {`
          #twitch-embed {
            box-sizing: border-box;
            background-color: rgb(255, 255, 255);
            border: 1px solid rgb(238, 238, 238);
            box-shadow: rgb(0 0 0 / 10%) 0px 8px 16px;
          }
          .twitch-video {
            height: 480px;
            width: 800px;
          }
          .twitch-video-md {
            height: 720px;
            width: 1280px;
          }
          .twitch-video-lg {
            height: 900px;
            width: 1600px;
          }
        `}
      </style>
      <div
        id="twitch-embed"
        className={` border border-dark ${
          Size == 'S' ? 'twitch-video' : Size == 'M' ? 'twitch-video-md' : Size=='L'?'twitch-video-lg':''
        }  ${twitchVideoStyle} `}></div>
      <div className={'py-3 d-flex flex-row align-items-end'}>
        <div>

        <p className={'mb-0 mx-4'}>Change Streamer: </p>
        <Input
          type="text"
          placeholder="Moikapy"
          value={newUser}
          onChange={async (e) => {
            setNewUser(e.target.value);
          }}
        />
        <Button
          onClick={() => {
            setUpdateStream(newUser);
            onPress(newUser);
          }}>
          Change
        </Button>
      
        </div>
      </div>
    </div>
  );
}
const TwitchVideo = dynamic(() => Promise.resolve(_TwitchVideo), {
  ssr: false,
});
export default TwitchVideo;
