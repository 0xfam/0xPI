import { useEffect, useState } from 'react';
import Select from '@/components/Select';
import { Input, Card, Field } from 'rimble-ui';
import { useRouter } from 'next/router';
import moifetch from 'moifetch';
import { DMButton } from '@/components/common';
import NFTSection from '@/components/NFTSection.js';
import { connect } from 'react-redux';
const fetch = require('node-fetch');
function Gallery({ collection = '0xpi' }) {
  const router = useRouter();
  const handleClick = (e) => {
    router.push(e);
  };
  const [assetState, setAssetState] = useState([]);
  const [orderDirectiontState, setOrderDirectionState] = useState('desc');
  const [offsetState, setOffsetState] = useState(0);
  const [orderByState, setOrderByState] = useState('listing_date');
  const [apply, applyState] = useState(0);

  useEffect(() => {
    const url = 'https://api.opensea.io/api/v1/assets?';

    // const options = {
    //   method: 'GET',
    //   qs: ,
    // };
    // {
    // order_by: 'listing_date',
    //   order_direction: 'desc',
    //   offset: '1',
    //   limit: '2 ',
    // collection: '0xpi',
    // }
    let order_direction = 'desc',
      order_by = 'listing_date',
      offset = 0,
      limit = 50;
    moifetch
      .GET(
        url +
          'order_direction=' +
          orderDirectiontState +
          '&' +
          `offset=${offsetState}` +
          '&limit=' +
          limit +
          '&order_by=' +
          orderByState +
          `&owner=${'0xa8D145Dd3003817dA1DC83F838Ee5088B65Acf2e'}`
      )
      .then(async (res) => res.data)
      .then((json) => {
        // console.log(
        //   json.assets.filter(
        //     ({ name, owner }) => name !== null && owner !== null
        //   )
        // );
        setAssetState(
          json.assets.filter(
            ({ name, owner }) => name !== null && owner !== null
          )
        );
      })
      .catch((err) => console.error('error:' + err));
  }, [apply]);
  return (
    <div
      className={
        'Collection d-flex flex-md-row flex-column align-items-start mt-2 pb-5'
      }>
      <style jsx>
        {`
          main.container-fluid {
            padding: 0;
          }
          .nft-card {
            width: 20rem;
            // max-height: 20rem;
            box-shadow: 10px 5px #000;
          }
          .nft-card-img {
            min-height: 6.25rem;
            width: 14.0625rem;
            height: 15.625rem;
          }
          .nft-card-text {
            height: 75px;
          }

          img {
            object-fit: contain;
          }
        `}
      </style>
      <div className={`d-flex flex-column`}>
        <p className="h4">Galley</p>
        <p>Our NFT's For Show</p>
      </div>
      <div
        className={
          'container d-flex flex-row flex-wrap justify-content-lg-between justify-content-center w-100 px-5 mx-auto mb-5'
        }>
        <NFTSection nft_array={assetState} />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  address: state.session.address,
});
export default connect(mapStateToProps)(Gallery);
