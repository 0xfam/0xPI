import { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import NFTSection from '@/components/NFTSection.js';
import _0xDao from '../dao';
const { getNFTCollections } = _0xDao;

function Collection({ collection = '0xpi' }) {
  const [assetState, setAssetState] = useState([]);

  useEffect(() => {
    (async () => setAssetState(await getNFTCollections(['0xpi', '0xpi-v2'])))();
  }, []);
  return (
    <div
      className={
        'Collection d-flex flex-md-row flex-column align-items-start mt-2 pb-5'
      }>
      <style jsx>
        {`
          main.container-fluid {
            padding: 0;
          }
          .nft-card {
            width: 20rem;
            // max-height: 20rem;
            box-shadow: 10px 5px #000;
          }
          .nft-card-img {
            min-height: 6.25rem;
            width: 14.0625rem;
            height: 15.625rem;
          }
          .nft-card-text {
            height: 75px;
          }

          img {
            object-fit: contain;
          }
        `}
      </style>
      <div className={`d-flex flex-column`}>
        <p className="h4">Collection</p>
        <p>Our Minted NFT's</p>
      </div>
      <div
        className={
          'container d-flex flex-row flex-wrap justify-content-lg-start justify-content-start w-100 px-5 mx-auto mb-5'
        }>
        <NFTSection nft_array={assetState} />
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  address: state.session.address,
});
export default connect(mapStateToProps)(Collection);
