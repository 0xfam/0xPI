import { useState, useEffect, useRef } from 'react';
import { useRouter } from 'next/router';
import { connect } from 'react-redux';
import { toggleShowAnnoucement, updateTwitchStreamUsername } from '@/actions';
import { getTokens } from 'lib';
import { DMButton } from '@/components/common';
import Hub from './hub';
import HomeHeader from '@/components/HomeHeader';

import NFTGallery from '@/components/NFTGallery';
import NFTModal from '@/components/NFTModal';
import NFTSection from '@/components/NFTSection';
import _0xDao from '../dao';
const { getNFTSByAddress } = _0xDao;
const Home = ({
  address,
  showAnnoucement,
  toggleShowAnnoucement,
  twitchStreamUsername,
  updateTwitchStreamUsername,
}) => {
  const router = useRouter();
  const handleClick = (e) => {
    router.push(e);
  };
  const [showModal, setShowModal] = useState(false);
  const [assetState, setAssetState] = useState([]);
  const [nftImgUrl, setNftImgUrl] = useState('');
  const [nftAnimationUrl, setNftAnimationUrl] = useState('');
  const [nftName, setNftName] = useState('');
  const [nftDescription, setNftDescription] = useState('');
  const [nftCollection, setNftCollection] = useState({});
  const [nftSaleOrder, setNftSaleOrder] = useState({});

  const [token, setTokenState] = useState([{ label: '', value: '' }]);
  useEffect(async () => {
    setTokenState(await getTokens());
  }, []);
  useEffect(() => {
    (async () => {
      var loading = true;
      var offset = 0;
      let arr = [];
      const returnArr = await getNFTSByAddress(
        '0x21ac9c0516b23460d3f6725fca0630116e85253b',
        offset
      );
      // while (loading) {
      //   if (returnArr.length == 0) {

      //   }
      // }
      setAssetState(returnArr);
    })();
  }, []);

  
  return (
    <div className="home container-fluid h-100 w-100 mx-auto pb-5 pt-3">
      <style global jsx>
        {`
          main.container-fluid {
            padding: 0;
          }
          .home {
            overflow-y: scroll;
            -ms-overflow-style: none; /* IE and Edge */
            scrollbar-width: none; /* Firefox */
          }
          .home::-webkit-scrollbar {
            display: none;
          }

          .home-header {
            // height: 100%;
            max-height: 32rem;
            // box-shadow: 10px 5px #000;
          }
          .hub-title {
            display: none !important;
          }
        `}
      </style>
      <HomeHeader
        homeHeaderStyle="home-header border border-dark position-relative"
        subTextStyle={'text-capitalize'}
        address={address}
      />
      <div className="col d-flex flex-column py-4">
        <p className={`h4 pt-3`}>Featured Artist: </p>
        {!(
          address !== null &&
          address !== undefined &&
          address.length !== 0 &&
          address[0] !== undefined
        ) ? (
          <div className="w-100">
            <p className={'text-capitalize'}>
              To have a full experience,{' '}
              <strong>
                <u>Please connect your Ethereum wallet</u>
              </strong>{' '}
              <svg
                width="1em"
                height="1em"
                viewBox="0 0 16 16"
                className="bi bi-arrow-up-right-circle-fill"
                fill="currentColor"
                xmlns="http://www.w3.org/2000/svg">
                <path
                  fillRule="evenodd"
                  d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM5.879 10.828a.5.5 0 1 1-.707-.707l4.096-4.096H6.5a.5.5 0 0 1 0-1h3.975a.5.5 0 0 1 .5.5V9.5a.5.5 0 0 1-1 0V6.732l-4.096 4.096z"
                />
              </svg>
            </p>
            <p className={`text-capitalize fnt-size-16`}>
              <i>
                Using the Connect Button will Link your wallet and begin the
                on-boarding process.
              </i>
            </p>
            <p className={`text-capitalize fnt-size-16`}>
              <i>
                If you don't have a metamask or a web3 enabled browser/wallet
                you will be guided through the process.
              </i>
            </p>
          </div>
        ) : null}
      </div>
      <hr />
      {/* <Hub /> */}
      <NFTSection nft_array={assetState} />
    </div>
  );
};

const mapStateToProps = (state) => ({
  address: state.session.address,
  showAnnoucement: state.session.showAnnoucement,
  twitchStreamUsername: state.session.twitchStreamUsername,
});

export default connect(mapStateToProps, {
  toggleShowAnnoucement,
  updateTwitchStreamUsername,
})(Home);
