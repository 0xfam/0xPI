import { useEffect, useState, useRef } from 'react';

import NFTSection from '@/components/NFTSection';
import { connect } from 'react-redux';
import _0xDao from '../dao';
const { getNFTSByAddress } = _0xDao;




function Hub({ address }) {
const [showModal, setShowModal] = useState(false);
const [assetState, setAssetState] = useState([]);
  useEffect(() => {
    var loading = true;
    var offset = 0;
    // while (loading)
      getNFTSByAddress(
        address,
        offset
      ).then((res) => {
        setAssetState(res);
        if (loading) {
          offset += 50;
        } else if (res.length == 0) {
          loading = false;
        }
      });
  }, []);


  return (
    <div
      className={
        'hub d-flex flex-md-row flex-column align-items-start mt-2 pb-5'
      }>
      <style jsx>
        {`
          main.container-fluid {
            padding: 0;
          }
        `}
      </style>
      <div className={`d-flex flex-column hub-title`}>
        <p className="h4">Hub</p>
        <p>Your NFT</p>
      </div>
      <NFTSection nft_array={assetState} />
    </div>
  );
}

const mapStateToProps = (state) => ({
  address: state.session.address,
});
export default connect(mapStateToProps)(Hub);
