import RSSFeed from '@/components/RSSFeed';
import { Link } from 'rimble-ui';
import { useRouter } from 'next/router';

const About = () => {
  const router = useRouter();
  return (
    <div className="d-flex flex-column mt-5">
      <style jsx>
        {`
          .about-container {
            max-width: 50rem;
          }
          .about-rss {
            // max-height: 40.625rem;
            // height: 100%;
          }
          .discord-link {
            width: 15rem !important;
          }
        `}
      </style>
      <div className="about-container d-flex flex-column p-5 mx-auto">
        <div className={`d-flex flex-column`}>
          <p className={'h5'}>Hello,</p>

          <p className={'text-capitalize'}>
            0xPI is currently a Web 2.0/3.0 Hybrid built by the best of
            programmers of <br className="d-none d-md-flex" />
            <strong>0x LLC</strong> with ♥ and the latest tools and API's to
            allow users to connect to the Ethereum and Ceramic Networks.
          </p>
          <p className={'text-capitalize'}>
            Our mission is to Normalize and Educate users through the use of
            NFT's while looking for ways to allow our decentralized ecosystem to
            be more environemtal and user friendly.
          </p>
        </div>

        <Link
          className="my-5 mr-auto d-flex flex-column"
          title="Go To Community Discord"
          href="https://discord.gg/DnbkrC8"
          target="_blank">
          Discord
          <img
            className="border border-dark discord-link"
            src="https://img.shields.io/discord/687169639712686097?style=for-the-badge&logo=discord"
            alt="Discord Icon"
          />
        </Link>
        {/* <Link
          className=" mr-auto"
          onClick={(e) => {
            e.preventDefault();
            router.push('/team');
          }}
          target="_blank"
          title="Go To Team Page">
          Meet The Team */}
        {/* </Link> */}
      </div>
      {/* 
      <div className={'about-rss mt-5 col overflow-scroll'}>
        <RSSFeed />
      </div> */}
    </div>
  );
};

export default About;
