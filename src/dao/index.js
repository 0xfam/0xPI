import moifetch from 'moifetch';
const _0xDao = {
  urls: {
    opensea_url: 'https://api.opensea.io/api/v1/assets?',
    etherscan_url: `https://api.etherscan.io/api?module=gastracker&action=gasoracle&apikey=${process.env.ETHERSCAN_API}`,
  },
  /**
   * WIP: WORK ON REMOVING FUNTIONS INTO OWN FILES AND
   * CALLING THEM HERE FOR ACCESS VIA DAO
   */
  getNFTSByAddress: async function (address = '', offset = 0) {
    try {
      let order_direction = 'desc',
        order_by = 'listing_date',
        limit = 50;
      let res = await moifetch.GET(
        _0xDao.urls.opensea_url +
          'order_direction=' +
          order_direction +
          '&' +
          `offset=${offset}` +
          '&limit=' +
          limit +
          '&order_by=' +
          order_by +
          `&owner=${address}`
      );
      return res.data.assets;
    } catch (error) {
      console.log(error);
    }
  },
  getNFTSByCollection: async function (collection = '') {
    try {
      let order_direction = 'desc',
        order_by = 'listing_date',
        offset = 0,
        limit = 50;
      let res = await moifetch.GET(
        _0xDao.urls.opensea_url +
          'order_direction=' +
          order_direction +
          '&' +
          `offset=${offset}` +
          '&limit=' +
          limit +
          '&order_by=' +
          order_by +
          `&collection=${collection}`
      );
      return res.data.assets.filter(
        ({ name, owner }) => name !== null && owner !== null
      );
    } catch (error) {
      console.log(error);
    }
  },
  getNFTCollections: async function (arr = []) {
    var _arr = [];
    for (var a of arr) {
      const array_item = await _0xDao.getNFTSByCollection(a);
      _arr.push(array_item);
    }
    return await _arr.flat();
  },
  truncateAddress: async function (address = '') {
    try {
      return `${address.substring(0, 6).toLowerCase()}...${address
        .substring(38, 42)
        .toLowerCase()}`;
    } catch (error) {
      console.log(`truncateAddress(): ${error}`);
    }
  },
  getGasPrices: async function () {
    try {
      const response = moifetch.GET(_0xDao.urls.etherscan_url);
      let gasPrices = await response.data;
      return gasPrices.result;
    } catch (error) {
      console.log('getQuote()', error);
    }
  },
};
export default _0xDao;
