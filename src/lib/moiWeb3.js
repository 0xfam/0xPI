/**
 *
 * @param {sting} recievecoin Coin being bought
 * @param {integer} value How Many Coins Bought
 * @param {integer} dec Number of Decimals Coin Uses
 */
export const getQuote = async (
  sendcoin = 'ETH',
  recievecoin = 'DAI',
  value = 0,
  dec = 18
) => {
  try {
    const response = await fetch(
      `https://api.0x.org/swap/v1/quote?buyToken=${recievecoin}&sellToken=${sendcoin}&buyAmount=${
        value * Math.pow(10, dec)
      }&feeRecipient=${
        process.env.ADMIN_ID
      }&buyTokenPercentageFee=0.00075&slippagePercentage=0.0377`
    );
    let quote = await response.json();
    return quote;
  } catch (error) {
    console.log('getQuote()', error);
  }
};
/**
 *
 * @param {object} trns
 */
export async function submitTransaction(trns) {
  try {
    await web3.eth.sendTransaction(trns);
  } catch (error) {
    console.log('submitTransaction()', error);
  }
}

export function truncateAddress(address) {
  try {
    return `${address.substring(0, 6).toLowerCase()}...${address
      .substring(38, 42)
      .toLowerCase()}`;
  } catch (error) {
    console.log(`truncateAddress(): ${error}`);
  }
}

export async function getTokens() {
  try {
    let tkns;
    const rowSize = 6;
    let tokensArray = [];
    let tokensGroupArray = [];
    let res = await fetch(`https://api.0x.org/swap/v1/tokens`);
    let tokens = await res.json();
    await tokens.records
      .filter(
        ({ symbol }) =>
          symbol !== 'GNT' &&
          symbol !== 'AION' &&
          symbol !== 'MLN' &&
          symbol !== 'ICN' &&
          symbol !== 'CREP' &&
          symbol !== 'ICX' &&
          symbol !== 'GUSD' &&
          symbol !== 'SWUSD' &&
          symbol !== 'YBCRV' &&
          symbol !== 'YUSDC' &&
          symbol !== 'YUSDT' &&
          symbol !== 'YTUSD'
      )
      .map((researcher, key) => {
        if (tokensArray.length == rowSize) {
          tokensGroupArray.push(tokensArray);
          tokensArray = [];
        }
        if (
          tokens.records.length == key + 1 &&
          !tokensArray.includes(researcher)
        ) {
          tokensArray.push({ label: researcher.symbol, value: researcher });
          tokensGroupArray.push(tokensArray);
        } else {
          tokensArray.push({ label: researcher.symbol, value: researcher });
        }
      });
    tkns = await tokensGroupArray.flat();

    return tkns;
  } catch (error) {
    setErrorState(JSON.stringify(error, null, 2));
  }
}

export async function getGasPrices() {
  //

  try {
    const response = await fetch(
      `https://api.etherscan.io/api?module=gastracker&action=gasoracle&apikey=${process.env.ETHERSCAN_API}`
    );
    let gasPrices = await response.json();
    return gasPrices.result;
  } catch (error) {
    console.log('getQuote()', error);
  }
}
